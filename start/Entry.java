package start;

public class Entry {
	
	private String name;
	private String definition;
	
	public Entry(String name, String definition) {
		this.name = name;
		this.definition = definition;
	}

	public String getName() {
		return name;
	}

	public String getDefinition() {
		return definition;
	}

}
