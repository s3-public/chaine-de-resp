package start;

public class Main {
	public static void main(String[] args) {
		Expressions expressions = new Expressions();
		expressions.rechercherEtEditerDuPlusPresAuPlusLoin("abcd");
		System.out.println();
		expressions.rechercherEtEditerDuPlusPresAuPlusLoin("tarpin");
		System.out.println();
		expressions.rechercherEtEditerDuPlusPresAuPlusLoin("cag");
		System.out.println();
		expressions.rechercherEtEditerDuPlusLoinAuPlusPres("ca");
	}
}
