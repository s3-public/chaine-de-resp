package start;

public class Expressions {
    
    private ExpressionLieu toulouse;
	private ExpressionLieu lorraine;
	private ExpressionLieu sud;
	
	public Expressions() {
		//definir les maillons de la chaine
		ExpressionLieu toulouse = new Toulouse();
		ExpressionLieu lorraine = new Lorraine();
		ExpressionLieu sud = new Sud();
	}
	
	public void rechercherEtEditerDuPlusPresAuPlusLoin(String lettres) {
		// afficher la recherche
		System.out.println("Recherche des expressions contenant " + lettres);
		
		//definir les maillons suivants
		toulouse.setSuivant(sud);
		sud.setSuivant(lorraine);
		lorraine.setSuivant(null);
		//appeler le premier maillon
		toulouse.recherche(lettres);
	}
	
	public void rechercherEtEditerDuPlusLoinAuPlusPres(String lettres) {
		// afficher la recherche
		System.out.println("Recherche des expressions contenant " + lettres);
		
		//definir les maillons suivants
		lorraine.setSuivant(sud);
		sud.setSuivant(toulouse);
		toulouse.setSuivant(null);
		//appeler le premier maillon
		lorraine.recherche(lettres);
	}
}
