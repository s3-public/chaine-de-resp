package start;

public class Sud extends ExpressionLieu{
	
	public Sud() {
		this.getExpressions().add(new Entry("Se gaver", "ce qui veut dire exceller, si vous d�ambulez du c�t� de N�mes."));
		this.getExpressions().add(new Entry("Tarpin", "le trop ou tr�s marseillais. C'est tarpin bon."));
		this.getExpressions().add(new Entry("Ca p�gue", "�a colle, tout simplement."));
		this.getExpressions().add(new Entry("Ca m'espante", "autrement dit, c'est surprenant."));
		this.getExpressions().add(new Entry("Ca m'enfade", "A comprendre tu m'�nerves."));
		this.getExpressions().add(new Entry("Peuch�re", "mon pauvre ou ma pauvre."));
		this.getExpressions().add(new Entry("Roum�guer", "signifie r�ler."));
		this.getExpressions().add(new Entry("�tre ensuqu�", "veut dire �tre endormi, assoupi, mou de la chique."));
		this.getExpressions().add(new Entry("Caguer", "ou plus commun�ment chier, d�f�quer."));
		this.getExpressions().add(new Entry("�tre quich�", "signifie �tre serr�."));
		this.getExpressions().add(new Entry("Boudiou", "le Mon Dieu du sudiste branch�."));
	}

	@Override
	public void recherche(String lettres) {
		System.out.println("Expressions du Sud...");
		super.recherche(lettres);
	}
}
