package start;

public class Lorraine extends ExpressionLieu {
	
	public Lorraine() {
		this.getExpressions().add(new Entry("Oh l�autre !",  "Tu rigoles !"));
		this.getExpressions().add(new Entry("Tu viens avec ?", "Viens-tu avec nous ?"));
		this.getExpressions().add(new Entry("Brimbelle", "Myrtille"));
		this.getExpressions().add(new Entry("Oh l�autre !",  "Tu rigoles !"));
		this.getExpressions().add(new Entry("Beuille", "un bleu"));
		this.getExpressions().add(new Entry("Faire bleu", "s�cher les cours"));
		this.getExpressions().add(new Entry("Ca caille", "Il fait froid"));
		this.getExpressions().add(new Entry("Chlass","Couteau"));
		this.getExpressions().add(new Entry("Clanche", "La poign�e de porte"));
		this.getExpressions().add(new Entry("Ca geht�s ?", "Ca va bien ?"));
		this.getExpressions().add(new Entry("Entre midi", "Entre midi et 14h"));
		this.getExpressions().add(new Entry("Flot", "noeud"));
		this.getExpressions().add(new Entry("Nareux", "Personne qui n�aime pas boire apr�s quelqu�un dans la m�me bouteille"));
		this.getExpressions().add(new Entry("Schnek", "Pain aux raisins"));
		this.getExpressions().add(new Entry("Schlapp", "Savate"));
		this.getExpressions().add(new Entry("Schlouc", "Une gorg�e"));
	}

	@Override
	public void recherche(String lettres) {
		System.out.println("Expressions de Lorraine...");
		super.recherche(lettres);
	}
}
