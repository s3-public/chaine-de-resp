package start;

import java.util.LinkedList;

public abstract class ExpressionLieu {
	
	private LinkedList<Entry> expressions;
	private ExpressionLieu suivant;
	
	public ExpressionLieu() {
		this.expressions = new LinkedList<Entry>();
	}
	
	public LinkedList<Entry> getExpressions() {
		return expressions;
	}
	
	public void setSuivant(ExpressionLieu suivant) {
		this.suivant = suivant;
	}
	
	public void recherche(String lettres) {
		for (Entry e:this.expressions) {
			String name = e.getName().toLowerCase();
			if (name.contains(lettres)) {
				System.out.println(e.getName() + " : " + e.getDefinition());
			}				
		}
		if(this.suivant != null) {
			suivant.recherche(lettres);
		}
	}
}
