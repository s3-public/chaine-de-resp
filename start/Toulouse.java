package start;

public class Toulouse extends ExpressionLieu{
	
	public Toulouse() {
		this.getExpressions().add(new Entry("Aimable", "gentil, agr�able. Elle est aimable, la ma�tresse ?"));
		this.getExpressions().add(new Entry("Bader", "rester bouche b�e devant. Ta soeur, elle arr�te pas de le bader."));
		this.getExpressions().add(new Entry("Banane", "mauvaise note. C'est la premi�re fois de l'ann�e que je prends pas une banane."));
		this.getExpressions().add(new Entry("Cagade",  "chiure, �chec. �a a �t� une cagade totale. De l'occitan cagada, chi�e."));
		this.getExpressions().add(new Entry("Cagnasse", "grosse chaleur, canicule, celle qui co�te des litres de sueur � chaque mouvement."));
		this.getExpressions().add(new Entry("Caguer", "Tu vas pas nous en caguer une pendule ! Il nous fait caguer, l'autre ! De l'occitan cagar, chier."));
		this.getExpressions().add(new Entry("D�ner", "repas de midi, d�je�ner. Venez vers midi pour d�ner ! De l'occitan dinnar."));
		this.getExpressions().add(new Entry("Empapaouter", "arnaquer, rouler. Je te l'ai empapaout� vite fait ! De l'occitan empapautar."));
		this.getExpressions().add(new Entry("Franchimand", "fran�ais du nord (de la Loire). Le probl�me avec les franchimands, c'est qu'ils se croient plus intelligents que toi."));
		this.getExpressions().add(new Entry("Garnir", "remplir un formulaire. Vous me garnissez encore cette feuille, et ce sera bon."));
		this.getExpressions().add(new Entry("Patac", "Il s'est pris un pat�c qui l'a ensuqu�*, et il s'est esplatern�."));
		this.getExpressions().add(new Entry("Que", "car, parce que. Couvre-toi que tu vas avoir froid. De l'occitan que, car."));
		this.getExpressions().add(new Entry("Pigne", "fruit de conif�re. Prends des pignes pour lancer le feu."));
		this.getExpressions().add(new Entry("Rien que", "Il est venu rien que lui. Rien que ? Oui, rien que lui !"));
		this.getExpressions().add(new Entry("Tavanard", "objet qui boudonne fort en allant vite. La moto, elle m'a d�pass� comme un tavanard."));
	}
	
	@Override
	public void recherche(String lettres) {
		System.out.println("Expressions Toulousaines...");
		super.recherche(lettres);
	}
}
